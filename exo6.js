// exo 6 faire une fonction qui calcule la moyenne d'age de tous les joueurs de toutes les équipes, mais qui peut aussi donner la moyenne d'age d'une seule équipe en fonction de son index entré en paramètre

import {createTeams} from './exo2.js';

function averageYearsOld(index ) {
    
    if(!index) {
     
        const playerAges = createTeams(10).map((team)=> team.players
                                                        .map((player)=>player.age))
                                      .reduce((acc,ages)=> acc.concat(ages));
        
        let sum = playerAges.reduce((acc,years)=> acc + years);
        
        
     
     return  Math.ceil(sum / playerAges.length)
        
    } else {
      
         const team_by_id = createTeams(10).filter((team)=>team.id == index)
                                           .map((team)=>team.players.map((player)=>player.age));
         
         let sum2 = team_by_id[0].reduce((acc,years)=> acc + years)/team_by_id[0].length;
        
         return  Math.ceil(sum2)
    }
    
}
console.log(averageYearsOld(7));