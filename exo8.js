import {createTeams} from './exo2.js';

// exo 8 : faire une fonction qui classe toutes les équipes selon le numéro unique qu'il y a dans leur nom, puis qui classe tous les joueurs selon leur age


function sortByIdNumber () {
    
    let teams = createTeams(10);
    
    let newteam = [];
    
    let team_sorted = [];
    
    let countryNumbers = teams.map((team)=> {
        return team.country.split("");
    } )
    
    let letters = countryNumbers.map((value,index)=> value.map((value2)=>{
        if(typeof parseInt(value,10) === "number"){
            return parseInt(value2,10)
        }
    })).map((value3)=>value3.filter((value4)=> isNaN(value4) === false));
    
    let uniqueNumber = letters.map((value5)=>value5.join("")).sort((a,b)=> a-b);
    
    letters.forEach((value,index)=> {
        newteam[index] = value.join("");
    })
    
    
    let Names = teams.map((value)=>value.name);
    
    uniqueNumber.forEach((value,index)=> {
                               
        let realindex = newteam.indexOf(value);
        
        team_sorted[index] = teams[realindex];
        
        });
    
     let  age_ordered = team_sorted.map((team)=>{
                        team.players = team.players.sort((a,b)=>a.age-b.age);
                        return team
     });
         
         
   
    return age_ordered
}



console.log(sortByIdNumber ())