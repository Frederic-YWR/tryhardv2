// exo 1 : créer une fonction 'generatePlayers' qui génère des objets 'player' dans un tableau. Ces objets doivent avoir comme attributs : 'firstName' de type string, 'lastName' de type string, 'age' de type number, 'position' de type number.
// Les attributs lastName et firstName doivent etre tirés au sort dans des tableaux comportant chacuns 20 noms et prénoms différents que vous choisirez vous même
// l'attribut age doit etre un nombre aléatoire entre 16 et 45
// l'attribt 'position' doit etre un nombre UNIQUE (interdit d'avoir 2 fois le même nombre dans le tableau)
// la fonction doit prendre en paramètre un nombre supérieur à 10 et générer autant d'objet que ce nombe ou exactement 10 si ce nombre est inférieur à 10.

const first_name = [
    
    'Isaac',
    'Emmanuel',
    'Hussein',
    'Louis',
    'Leo',
    'Kolinda',
    'Jennifer',
    'Marie',
    'Charlize',
    'Mikasa',
    'Barry',
    'Barack',
    'Clark',
    'Anakin',
    'Mohamed',
    'Alyssa',
    'Serena',
    'Mereleona',
    'Valérie',
    'Coralie',
    
];

const last_name = [
    
    'Newton',
    'Lasker',
    'Bolt',
    'Ampère',
    'Messi',
    'Grabar-Kitarović',
    'Lopez',
    'Curie',
    'Tenron',
    'Hackerman',
    'Allen',
    'Obama',
    'Kent',
    'Skywalker',
    'Ali',
    'Milano',
    'Williams',
    'Vermillon',
    'Begue',
    'Mallet',
    
    
];

function createRandomNumber(min, max) {
    
  return Math.floor(min + Math.random() * (max - min));
    
}

function getValue(array) {
    
  return array[createRandomNumber(0, array.length)];
    
}
let generatePlayers = (items)=> {
    
    const players = [];
    
    for(let i = 0 ; i < items ; i++) {
        
        let playerKey = "player"+i;
        let obj = {
            firstname:getValue(first_name),
            lastname:getValue(last_name),
            age:createRandomNumber(16,45 ),
            position:i+1;
        };
        players.push(obj);
    }
    
    return players
} ;


export function createPlayers(items) {
 
    return generatePlayers(items)
}


console.log(generatePlayers(15));

