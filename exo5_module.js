// exo 5 : Créer une fonction qui prend en paramètre un numéro entre 0 et le nombre max d'équipe générées et qui renvoi l'équipe situé à cet index.
// Puis afficher les numéros des joueurs dans un console log sous la forme d'un triangle partant du plus petit numéro de joueurs au plus gros
// exemple :
//                          (1) -> 1 seul numéro
//                       (3)(5) ->  2 numéros
//                  (9)(12)(13) -> 3 numéros
//                etc.


function sigma (n) {
    
    let somme = 0;
    
    for (let i = 1;i <= n ;i++) {
        
       somme = somme + i;
    }
    
    return somme
}

function compare(value) {
    
  for(let i=0;i<value;i++){
      if(value >sigma(i-1) && value< sigma(i)){
          
          return i - 1;
      } else if(value === sigma(i)) {
          return i
      }
  }
}


function circle (str) {
    
    return "("+str+")";
}


function slice_array (n,arr) {
    
    let size = arr.length;
    let regular = compare(size);

    if(n < regular ) {
    
        return arr.slice(sigma(n-1),sigma(n))
     
    } else {
      
        return  arr.slice(sigma(n-1));
    }
    
    return
    
}



function showNumbers(array) {
    

    
    let limit = compare(array.length);
    
    for(let i=1; i < limit + 1; i++){
        
        console.log(slice_array(i,array).map((value)=>circle(value)).join(""));
    }
    
  return  
}



export function triangle(arr) {
    
    return showNumbers(arr)
}