// exo 2 : créer une fonction 'generateTeams' qui génère des objets 'team' dans un tableau. Ces objets doivent avoir comme attributs : 'name' de type string, 'country' de type string, 'subscribers' de type number, 'players' du même type que les objets générés à l'exercice 1.
// L' attribut name doit etre tirés au sort un tableau  20 noms de villes différents que vous choisirez vous même, concaténé à un nombre aléatoire unique compris entre 1 et 100 (ce nombre ne doit pas pouvoir apparaitre 2 fois, exemple si on créé paris1, il ne peut pas y avoir Niort1 et ces nombres ne doivent pas se suivre).
// Pareil pour l'attribut 'country' mais avec de noms de pays)
// l'attribut 'subscribers' doit etre un nombre aléatoire entre 1000 et 100000 (mais attention, ce doit être un nombre divisible par 100).
// l'attribt 'players' doit etre un tableau dont la longueur sera determiné par l'argument size que l'on passera à la fonction. Sachant que ce nombre doit etre au minimum de 10.

import {createPlayers} from './exo1.js';

const towns = [
    
    'Paris',
    'Londres',
    'Berlin',
    'Madrid',
    'Lisbonne',
    'Rome',
    'Washington',
    'Montreal',
    'Mexico',
    'La Havane',
    'Saint Domingue',
    'Buenos Aires',
    'Alger',
    'Tunis',
    'Abidjan',
    'Accra',
    'Bamako',
    'Libreville',
    'Tokyo',
    'Séoul',
 
    
    
];

const lands = [
    
    'France',
    'Angleterre',
    'Allemagne',
    'Espagne',
    'Portugal',
    'Italie',
    'Etats-Unis',
    'Canada',
    'Mexique',
    'Cuba',
    'République Dominicaine',
    'Argentine',
    'Algérie',
    'Tunisie',
    'Cote d \'Ivoire',
    'Ghana',
    'Mali',
    'Congo',
    'Japon',
    'Corée du Sud',
    
]
 
function createRandomNumber(min, max) {
    
  return Math.floor(min + Math.random() * (max - min));
    
}
function getValue(array) {
    
  return array[createRandomNumber(0, array.length)];
    
}

let generateTeams = (items)=> {
    
    const CityIndex = test(items);
    
    const CountryIndex = test(items);
    
    const teams = [];
    
    for(let i = 0 ; i < items ; i++) {
        
        let obj = {
            id:i+1,
            city:getValue(towns)+CityIndex[i],
            country:getValue(lands)+CountryIndex[i],
            subscribers: 100*createRandomNumber(10,1000),
            players:createPlayers(createRandomNumber(10, 20))
        };
        teams.push(obj);
    }
    
    return teams
} ;




function test(number) {
   let array = [];
   let nextNumber;

function countdown (fromnumber) {

    let tirage = createRandomNumber(1, 100);
    let twin = array.some((value)=> value === tirage);
    if ( twin === true) {
        nextNumber =  fromnumber; 
    } else {
         array.push(tirage);
           nextNumber =  fromnumber - 1;
    }
    
    if (nextNumber > 0 ) {
        
        countdown(nextNumber);
    } 
  
}
    countdown(number);
    
    return array
}


function test2(number,max) {
let array = [];
let nextNumber;

function countdown (fromnumber) {

    let tirage = createRandomNumber(1,max);
    let twin = array.some((value)=> value === tirage);
    if ( twin === true) {
        nextNumber =  fromnumber; 
    } else {
         array.push(tirage);
           nextNumber =  fromnumber - 1;
    }
    
    if (nextNumber > 0 ) {
        
        countdown(nextNumber);
    } 
  
}
    countdown(number);
    
    return array
}
function maxArray(arr) {
    arr.reduce(function(a,b) {
  return Math.max(a, b);
});
}
export function createTeams(items) {
 
    return generateTeams(items)
}
export function randomArray(item,max) {
 
    return test2(item,max)
}
console.log(generateTeams(15));