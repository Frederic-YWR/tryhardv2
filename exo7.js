// exo 7 : faire une fonction qui permet de transférer plusieurs joueurs d'une équipe A vers une équipe B en se servant des numéros des joueurs et des index des équipes. La fonction doit afficher le tableau des équipes avec leur nouvelle organisation

import {createTeams} from './exo2.js';

import {randomArray} from './exo2.js';

function createRandomNumber(min, max) {
    
  return Math.floor(min + Math.random() * (max - min));
    
}


function transfert (NumberOfteams) {
    
    let teams = createTeams(NumberOfteams);
    
    let indexTeamATeamB = randomArray(2,NumberOfteams);
    
    let indexTeamA = indexTeamATeamB[0];
    
    let indexTeamB = indexTeamATeamB[1];
    
    let TeamA = teams.find((team)=> team.id == indexTeamA);
    
    let TeamB = teams.find((team)=> team.id == indexTeamB);
    
    let NumberOfPlayersToExchange = createRandomNumber(1,TeamA.players.length-1)
    
    let PlayersToExchange = randomArray(NumberOfPlayersToExchange,TeamA.players.length);
    
    let teamArrIndexA = teams.findIndex((team)=> team.id == indexTeamA);
    
    let teamArrIndexB = teams.findIndex((team)=> team.id == indexTeamB);
    
    console.log(teams,TeamA,TeamB);
    
    PlayersToExchange.forEach((value)=> {
        
         let TeamBIndexes = TeamB.players.map((player)=>player.position).reduce((a,b)=>Math.max(a, b));
         let player = {...TeamA.players.find((player_loop)=> player_loop.position == value)};
         player.position = TeamBIndexes+1;
         teams[teamArrIndexB].players.push(player);
         let test = TeamA.players.findIndex((player_test)=> player_test.position == value);
         TeamA.players.splice(test,1);
         
    })
    
    return teams
    
}



console.log(transfert(10));