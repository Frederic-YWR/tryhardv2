// exo 3 : créer une fonction qui créé la liste de tous les joueurs de toutes les équipes qui ont exactement une position passée en paramètre

import {createTeams} from './exo2.js';

function getPlayersAtPosition (ranking) {
    
    let team = createTeams(10);
    
    let players = team.map((teams)=> teams.players)
                      .reduce((acc,player)=> acc.concat(player),[])
                      .filter((player2)=> player2.position === ranking);
    
    return players
}

console.log(getPlayersAtPosition (10));

// exo 3 bis : créer une fonction qui renvoie le nombre de joueurs de toutes les équipes dont l'âge est égale à celui passé en paramètre de la fonction

function getPlayerWithAge (years) {
    
    let team = createTeams(10);
    
    let players = team.map((teams)=> teams.players)
                      .reduce((acc,player)=> acc.concat(player),[])
                      .filter((player2)=> player2.age === years);
    
    if (players.length === 0 ) {
        
          return "Aucun résultat"
        
    } else {
        
          return players
    }
    
    
}

console.log(getPlayerWithAge (35));