
// exo 5 : Créer une fonction qui prend en paramètre un numéro entre 0 et le nombre max d'équipe générées et qui renvoi l'équipe situé à cet index.
// Puis afficher les numéros des joueurs dans un console log sous la forme d'un triangle partant du plus petit numéro de joueurs au plus gros
// exemple :
//                          (1) -> 1 seul numéro
//                       (3)(5) ->  2 numéros
//                  (9)(12)(13) -> 3 numéros
//                etc.

import {createTeams} from './exo2.js';
import {triangle} from './exo5_module.js';

function selectTeambyIndex (index) {
    
   
    
    let teamPlayers = createTeams(15).find((team) => team.id === index )
                                      .players.map((player)=> player.position);
    
   return teamPlayers
}


console.log(triangle(selectTeambyIndex(5)));